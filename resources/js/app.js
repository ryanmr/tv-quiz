(function(){

  // internal settings

  var duration = 20;
  var durations = {
    initial: Math.floor(duration * 1000),
    review: (Math.floor(duration/3) + 1) * 1000
  };

  var app = {};
  var time = $('#time');

  function fy_shuffle(array) {
    // implementation from http://stackoverflow.com/a/6274398
    var counter = array.length, temp, index;
    while (counter > 0) {
        index = Math.floor(Math.random() * counter);
        counter--;
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
  }

  function format(question) {
    // format the question object
    var q = {question: question.question, answers: []};

    // add each incorrect answer to the structure
    question.incorrect.forEach(function(i){
      q.answers.push({text: i, state: false});
    });

    // add the correct answer
    q.answers.push({text: question.correct, state: true});

    // key state implies which answer is the correct choice

    // randomize the answers in the list
    q.answers = fy_shuffle(q.answers);

    return q;
  }

  function get_random(array) {
    var index = Math.floor(Math.random() * (array.length));
    return array[index];
  }

  function get_data() {
    $.ajax({
      type: 'get',
      url: 'quiz.quiz.json',
      success: (data) => {
        app.data = JSON.parse(data);
        app.history = [];
        update();
      }
    });
  }

  function start() {
    get_data();
  }

  function select_question() {
    var q = get_random(app.data);
    vue.question = format(q);
  }

  function update() {
    select_question();

    visualize();
  }

  function visualize() {
    vue.reveal = false;

    visualize_reset();

    // 1. animate the bar; grow width and change color
    time.animate({
      'width': '100%', 'background-color': '#FF9100'}, durations.initial, 'linear', () => {

        // 2. a small delay until the animation is acknowledged
        setTimeout(() => {
          // 3. tell vue to show the answers
          vue.reveal = true;

          // 4. this delay helps the animation library catch up
          setTimeout(() => {

            // 5. animate the bar's opacity
            time.animate({'opacity': 0}, durations.review, 'linear', () => {
              // 6. finally, update everything, after another delay so it has time to catch up
              setTimeout(() => {update();}, 1000);
            });

          }, 500);

        }, 500);

    });

  }

  function visualize_reset() {
    time.css('width', '0%');
    time.css('background-color', '#000000');
    time.css('opacity', 1);
  }

  var vue = new Vue({
    el: '#question',

    data: {

      reveal: false,

      question: {
        question: null,
        answers: []
      }

    },

    ready: function() {
      console.log('ready');
      start();
    }

  });

})();
