/**
 *
 * An unusual proposition here.
 *
 * Using node to make a tiny console utility
 * to parse my content specifically.
 *
 * q: ~~~
 * + correct ~~~
 * - wrong ~~~
 * - wrong ~~~
 * - wrong ~~~
 *
 * This q-block can repeat as many
 * times as desired.
 *
 */

fs = require('fs');

function split_data(data) {
  return data.split("\n");
}

function is_question(line) {
  return line.charAt(0) == 'q';
}

function is_correct_answer(line) {
  return line.charAt(0) == '+';
}

function is_incorrect_answer(line) {
  return line.charAt(0) == '-';
}

var cli_args = process.argv.slice(2);
var file = cli_args[0];

fs.stat(file, function(err, stats){
  if (err) {
    console.log(err);
    return false;
  }
  if (stats.isFile()) {

    console.log(file + ' exists.');

    fs.readFile(file, 'utf8', function (err, data) {
      if (err) {
        console.log(err);
        return false;
      }

      console.log(file + ' is ' + (Math.round(data.length*100/1024)/100) + ' kB long.');

      // parse this stuff
      var lines = split_data(data);

      // clean off the whitespace
      lines.map(function(line){
        return line.trim();
      })

      // remove empty lines
      lines = lines.filter(function(line){
        return line != "";
      });

      // questions array, primary structure
      var questions = [];
      // maintains which question should get upcoming answers
      var current = null;

      lines.forEach(function(line){
        if (is_question(line)) {
          // create an object structure for the data
          // and collect into the questions array
          // as well as maintaining which question is currently
          // being processed
          var q = {question: line.substring(2).trim(), correct: null, incorrect: []};
          questions.push(q);
          current = q;
        }

        if (is_correct_answer(line)) {
          current.correct = line.substring(2).trim();
        }

        if (is_incorrect_answer(line)) {
          current.incorrect.push(line.substring(2).trim());
        }
      });

      console.log(questions.length + ' questios parsed.');

      var json = JSON.stringify(questions, null, "\t");

      fs.writeFile('quiz.quiz.json', json, function(err) {
        if (err) {
          console.log(err)
          return false;
        }
        console.log('quiz.json saved!');
      });

    });

  }
});
